save_params <- function(path_param){
	res = ""	# 	Page : global_params
		res = paste0(res , paste("global_params:", paste0('"', input$selectglobal_params, '"'), "\n", sep = " "))
		if(!is.na(as.numeric(input$results_dir))) {
			res = paste0(res, paste("results_dir:", format(input$results_dir, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("results_dir:", paste0('"', input$results_dir, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$sample_dir))) {
			res = paste0(res, paste("sample_dir:", format(input$sample_dir, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("sample_dir:", paste0('"', input$sample_dir, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$SeOrPe))) {
			res = paste0(res, paste("SeOrPe:", input$SeOrPe, "\n", sep = " "))
		} else {
			res = paste0(res, paste("SeOrPe:", paste0('"', input$SeOrPe, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$memo))) {
			res = paste0(res, paste("memo:", format(input$memo, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("memo:", paste0('"', input$memo, '"'), "\n", sep = " "))
		}	

	# 	Page : tlex3
		res = paste0(res , paste("tlex3:", paste0('"', input$selecttlex3, '"'), "\n", sep = " "))
		res = paste0(res, paste("tlex3_te_list_select:", paste0('"', input$tlex3_te_list_select, '"'), "\n", sep = " "))
		if(input$tlex3_te_list_select == "server") {
			res = paste0(res, paste("tlex3_te_list:", paste0('"', input$tlex3_te_list_server, '"'), "\n", sep = " "))
		}
		if(input$tlex3_te_list_select == "local"){
			res = paste0(res, paste("tlex3_te_list:", paste0('"', input$tlex3_te_list_local$datapath, '"'), "\n", sep = " "))
		}	

		res = paste0(res, paste("tlex3_te_annotations_select:", paste0('"', input$tlex3_te_annotations_select, '"'), "\n", sep = " "))
		if(input$tlex3_te_annotations_select == "server") {
			res = paste0(res, paste("tlex3_te_annotations:", paste0('"', input$tlex3_te_annotations_server, '"'), "\n", sep = " "))
		}
		if(input$tlex3_te_annotations_select == "local"){
			res = paste0(res, paste("tlex3_te_annotations:", paste0('"', input$tlex3_te_annotations_local$datapath, '"'), "\n", sep = " "))
		}	

		res = paste0(res, paste("tlex3_genome_fasta_select:", paste0('"', input$tlex3_genome_fasta_select, '"'), "\n", sep = " "))
		if(input$tlex3_genome_fasta_select == "server") {
			res = paste0(res, paste("tlex3_genome_fasta:", paste0('"', input$tlex3_genome_fasta_server, '"'), "\n", sep = " "))
		}
		if(input$tlex3_genome_fasta_select == "local"){
			res = paste0(res, paste("tlex3_genome_fasta:", paste0('"', input$tlex3_genome_fasta_local$datapath, '"'), "\n", sep = " "))
		}	

		if(!is.na(as.numeric(input$tlex3_species))) {
			res = paste0(res, paste("tlex3_species:", format(input$tlex3_species, scientific=F), "\n", sep = " "))
		} else {
			res = paste0(res, paste("tlex3_species:", paste0('"', input$tlex3_species, '"'), "\n", sep = " "))
		}	

	a = yaml.load_file("/workflow/params.total.yml", handlers=list("float#fix"=function(x){ format(x,scientific=F)}, int = function(x){return(as.character(x))}))
	p = a[["params"]]
	a["params"] = NULL
	b = yaml.load(res, handlers=list("float#fix"=function(x){ format(x,scientific=F)}, int = function(x){return(as.character(x))}))
	pnotb = subset(names(p), !(names(p)%in%names(b)))
	d = list()
	d$params = c(p[pnotb],b)
	logical = function(x) {
		result <- ifelse(x, "True", "False")
		class(result) <- "verbatim"
		return(result)
	}
	d = c(d,a)
	write_yaml(d,path_param,handlers=list(logical = logical,"float#fix"=function(x){ format(x,scientific=F) }))
	}

force_rule <- function(force_from){
	if (input$force_from=="none"){
		return("")
	}
	else if (input$force_from=="all"){ return("--forcerun all") }
	else {
		params = yaml.load_file(paste0(input$results_dir,"/params.yml"), handlers=list("float#fix"=function(x){ format(x,scientific=F)}))
		outputs = params[["outputs"]]
		tool = params[["params"]][[force_from]]
		if (length(outputs[[tool]])==1)
			rule = names(outputs[[tool]])[[1]]
		else{
			rule = names(outputs[[tool]])[[grep(input$SeOrPe,names(outputs[[tool]]))]]
		}
		return(paste0("--forcerun ",rule))
	}
}
#' Event when use RULEGRAPH button
observeEvent({c(input$sidebarmenu,input$refresh_rg)}, {

	if(input$sidebarmenu=="RULEGRAPH"){
		input_list <- reactiveValuesToList(input)
		toggle_inputs(input_list,F,F)
		path_param <- paste0(input$results_dir,"/params.yml")

		save_params(path_param)
		i = sample.int(1000,size = 1)

		system(paste0("rm ",input$results_dir,"/rulegraph*"))

		outUI = tryCatch({
			system(paste0("snakemake -s /workflow/Snakefile --configfile ",input$results_dir,"/params.yml -d ",input$results_dir," all --rulegraph 1> ",input$results_dir,"/rulegraph",i,".dot"),intern=T)
			system(paste0("cat ",input$results_dir,"/rulegraph",i,".dot | dot -Tsvg -Gratio=0.75 > ",input$results_dir,"/rulegraph",i,".svg"),intern=T)
			tagList(img(src = paste0("results/rulegraph",i,".svg") ,alt = "Rulegraph of Snakemake jobs",style="max-width: 100%;height: auto;display: block;margin: auto"))},
		error = function(e){
			system(paste0("touch ",input$results_dir,"/logs/workflow_end.error"),wait = T)
			return(tags$p(paste0("error : ",e$message)))},
		warning = function(w){
			system(paste0("touch ",input$results_dir,"/logs/workflow_end.error"),wait = T)
			return(tags$p(paste0("error : ",w$message)))})
		addResourcePath("results", input$results_dir)
		output$RULEGRAPH_svg = renderUI(outUI)
		toggle_inputs(input_list,T,F)
}})
#' Event when use RunPipeline button
observeEvent(input$RunPipeline, {

	rv$running = T
	input_list <- reactiveValuesToList(input)
	toggle_inputs(input_list,F,F)
	updateTabsetPanel(session, "sidebarmenu", selected = "run_out")
	path_param <- paste0(input$results_dir,"/params.yml")

		save_params(path_param)


	outUI = tryCatch({
		if (!dir.exists(paste0(input$results_dir,"/logs"))){
			dir.create(paste0(input$results_dir,"/logs"))
		}
		if (!file.exists(paste0(input$results_dir,"/logs/runlog.txt"))){
			file.create(paste0(input$results_dir,"/logs/runlog.txt"))
		}
		system(paste0("touch ",input$results_dir,"/logs/workflow.running"),wait = T)
			system(paste0("snakemake -s /workflow/Snakefile --configfile ",input$results_dir,"/params.yml -d ",input$results_dir," all --rulegraph | dot -Tpng -Gratio=0.75 > ",input$results_dir,"/Rule_graph_mqc.png"))
		force = force_rule(input$force_from)
		rerun = if (input$rerun_incomplete) "--rerun-incomplete" else ""
		system2("python3",paste0("-u -m snakemake -s /workflow/Snakefile --configfile ", paste0(input$results_dir,"/params.yml") ,	" -d ", input$results_dir ,	" --cores ", input$cores, " all ", force, " ",rerun),wait = FALSE, stdout = paste0(input$results_dir,"/logs/runlog.txt"), stderr = paste0(input$results_dir,"/logs/runlog.txt"))
		tags$iframe(src="results/multiqc_report.html",width="100%", height="900px")},
		error = function(e){
			system(paste0("touch ",input$results_dir,"/logs/workflow_end.error"),wait = T)
			return(tags$p(paste0("error : ",e$message)))},
		warning = function(w){
			system(paste0("touch ",input$results_dir,"/logs/workflow_end.error"),wait = T)
			return(tags$p(paste0("error : ",w$message)))})
		})

		shinyDirChoose(input, "shinydir_results_dir", root=c(Results="/Results"),session = session)
		observeEvent({parseDirPath(c(Results="/Results"),input$shinydir_results_dir)},{
			updateTextInput(session,"results_dir",value = parseDirPath(c(Results="/Results"),input$shinydir_results_dir))
		})

		shinyDirChoose(input, "shinydir_sample_dir", root=c(Data="/Data",Results="/Results"),session = session)
		observeEvent({parseDirPath(c(Data="/Data",Results="/Results"),input$shinydir_sample_dir)},{
			updateTextInput(session,"sample_dir",value = parseDirPath(c(Data="/Data",Results="/Results"),input$shinydir_sample_dir))
		})

		shinyFileChoose(input, "shinyfiles_tlex3_te_list", root=c(Data="/Data",Results="/Results"),session = session)
		observeEvent({parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_tlex3_te_list)$datapath[1]},{
			updateTextInput(session,"tlex3_te_list_server",value = parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_tlex3_te_list)$datapath[1])
		})

		shinyFileChoose(input, "shinyfiles_tlex3_te_annotations", root=c(Data="/Data",Results="/Results"),session = session)
		observeEvent({parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_tlex3_te_annotations)$datapath[1]},{
			updateTextInput(session,"tlex3_te_annotations_server",value = parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_tlex3_te_annotations)$datapath[1])
		})

		shinyFileChoose(input, "shinyfiles_tlex3_genome_fasta", root=c(Data="/Data",Results="/Results"),session = session)
		observeEvent({parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_tlex3_genome_fasta)$datapath[1]},{
			updateTextInput(session,"tlex3_genome_fasta_server",value = parseFilePaths(c(Data="/Data",Results="/Results"),input$shinyfiles_tlex3_genome_fasta)$datapath[1])
		})


