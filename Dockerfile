FROM mbbteam/mbb_workflows_base:latest as alltools

RUN cd /opt/biotools \
 && git clone https://github.com/GonzalezLab/T-lex3.git \
 && mv T-lex3/tlex-open-v3.0.pl bin/tlex \
 && chmod +x bin/tlex

RUN apt-get update \
 && apt-get install -y perl unzip bzip2 libkrb5-3 \
 && wget http://security.ubuntu.com/ubuntu/pool/main/libp/libpng/libpng12-0_1.2.54-1ubuntu1.1_amd64.deb && dpkg -i libpng12-0_1.2.54-1ubuntu1.1_amd64.deb \
 && apt-get clean -y

RUN cd /opt/biotools \
 && wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/rmblast/2.2.28/ncbi-rmblastn-2.2.28-x64-linux.tar.gz \
 && wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.9.0/ncbi-blast-2.9.0+-x64-linux.tar.gz \
 && tar -xzf ncbi-rmblastn* \
 && tar -xzf ncbi-blast* \
 && mv ncbi*/bin/* bin \
 && rm -rf ncbi*

RUN cd /opt/biotools  \
 && wget http://tandem.bu.edu/trf/downloads/trf407b.linux64 \
 && mv trf*.linux64 bin/trf \
 && chmod +x bin/trf

ENV PATH $PATH:/opt/biotools/RepeatMasker 
RUN cd /opt/biotools \
 && wget http://www.repeatmasker.org/RepeatMasker-open-4-0-9-p2.tar.gz \
 && tar -xzf RepeatMasker-open-4-0-9-p2.tar.gz \
 && rm -rf RepeatMasker-open-4-0-9-p2.tar.gz \
 && perl -0p -e 's/\/usr\/local\/hmmer/\/usr\/bin/g;' -e 's/\/usr\/local\/rmblast/\/opt\/biotools\/bin/g;' -e 's/DEFAULT_SEARCH_ENGINE = "crossmatch"/DEFAULT_SEARCH_ENGINE = "ncbi"/g;' -e 's/TRF_PRGM = ""/TRF_PRGM = "\/opt\/biotools\/bin\/trf"/g;' RepeatMasker/RepeatMaskerConfig.tmpl > RepeatMasker/RepeatMaskerConfig.pm \
 && cd RepeatMasker \
 && perl -i -0pe 's/^#\!.*perl.*/#\!\/usr\/bin\/env perl/g' RepeatMasker DateRepeats ProcessRepeats RepeatProteinMask DupMasker util/queryRepeatDatabase.pl util/queryTaxonomyDatabase.pl util/rmOutToGFF3.pl util/rmToUCSCTables.pl \
 && cpan Text::Soundex

ENV PATH $PATH:/opt/biotools/SHRiMP_2_2_3 
RUN cd /opt/biotools \
 && wget http://compbio.cs.toronto.edu/shrimp/releases/SHRiMP_2_2_3.lx26.x86_64.tar.gz \
 && tar -xzf SHRiMP_2_2_3.lx26.x86_64.tar.gz \
 && rm -rf SHRiMP_2_2_3.lx26.x86_64.tar.gz

ENV PATH $PATH:/opt/biotools/blat36 
RUN cd /opt/biotools \
 && mkdir blat36 \
 && wget https://hgwdev.gi.ucsc.edu/~kent/exe/linux/blatSuite.36.zip \
 && unzip blatSuite.36.zip -d /opt/biotools/blat36 \
 && rm -rf blatSuite.36.zip

RUN cd /opt/biotools \
 && wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2 \
 && tar -xvjf samtools-1.9.tar.bz2 \
 && cd samtools-1.9 \
 && ./configure && make \
 && cd .. \
 && mv samtools-1.9/samtools bin/samtools \
 && rm -r samtools-1.9 samtools-1.9.tar.bz2

RUN cd /opt/biotools \
 && wget https://github.com/samtools/bcftools/releases/download/1.9/bcftools-1.9.tar.bz2 \
 && tar -xvjf bcftools-1.9.tar.bz2 \
 && cd bcftools-1.9 \
 && ./configure --prefix=/opt/biotools \
 && make -j 10 \
 && make install \
 && mv bcftools /opt/biotools/bin/ \
 && cd .. && rm -r bcftools-1.9.tar.bz2 bcftools-1.9

RUN cd /opt/biotools \
 && wget https://github.com/lh3/bwa/releases/download/v0.7.17/bwa-0.7.17.tar.bz2 \
 && tar -xvjf bwa-0.7.17.tar.bz2 \
 && cd bwa-0.7.17 \
 && make -j 10 \
 && mv bwa ../bin/ \
 && cd .. \
 && rm -r bwa-0.7.17 bwa-0.7.17.tar.bz2

RUN apt-get update -q \
 && apt-get install -y -qqq build-essential pkg-config autoconf \
 && cd /tmp \
 && wget https://github.com/agordon/libgtextutils/releases/download/0.7/libgtextutils-0.7.tar.gz \
 && tar -xzf libgtextutils-0.7.tar.gz \
 && cd libgtextutils-0.7 \
 && ./reconf \
 && ./configure \
 && make CXXFLAGS='-std=c++03' \
 && make install \
 && cd /tmp \
 && wget https://github.com/agordon/fastx_toolkit/releases/download/0.0.14/fastx_toolkit-0.0.14.tar.bz2 \
 && tar -xjf fastx_toolkit-0.0.14.tar.bz2 \
 && cd fastx_toolkit-0.0.14 \
 && ./reconf \
 && ./configure \
 && make \
 && make install \
 && apt-get remove -y -qqq build-essential pkg-config autoconf \
 && apt-get autoremove -y -qqq \
 && apt-get clean -y -qqq \
 && cd /tmp \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /tmp/*

RUN apt-get update -q \
 && apt-get install -y emboss \
 && apt-get clean -y

RUN cd /tmp \
 && wget http://bioinfo.ut.ee/download/dl.php?file=28 -O fastagrep.tar.gz \
 && tar -xzf fastagrep.tar.gz \
 && mv ./fastagrep_v2.0_64bit_linux_2_6 /opt/biotools/bin/fastagrep \
 && rm -rf fastagrep.tar.gz

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#This part is necessary to run on ISEM cluster
RUN mkdir -p /share/apps/bin \
 && mkdir -p /share/apps/lib \
 && mkdir -p /share/apps/gridengine \
 && mkdir -p /share/bio \
 && mkdir -p /opt/gridengine \
 && mkdir -p /export/scrach \
 && mkdir -p /usr/lib64 \
 && ln -s /bin/bash /bin/mbb_bash \
 && ln -s /bin/bash /bin/isem_bash \
 && /usr/sbin/groupadd --system --gid 400 sge \
 && /usr/sbin/useradd --system --uid 400 --gid 400 -c GridEngine --shell /bin/true --home /opt/gridengine sge

EXPOSE 3838
CMD ["Rscript", "-e", "setwd('/sagApp/'); shiny::runApp('/sagApp/app.R',port=3838 , host='0.0.0.0')"]


FROM alltools

COPY files /workflow
COPY sagApp /sagApp

